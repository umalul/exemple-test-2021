import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';
import { PredictService } from '../predict.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  userId;

  customers:Customer[];
  customers$;
  addCustomerFormOpen;
  rowToEdit:number = -1; 
  customerToEdit:Customer = {name:null, education:null, income:null};

  add(customer:Customer){
    this.customersService.addCustomer(this.userId, customer.name, customer.education, customer.income)
  }
  
  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.education = this.customers[index].education;
    this.customerToEdit.income = this.customers[index].income;
    this.rowToEdit = index; 
  }

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.customersService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.education,this.customerToEdit.income);
    this.rowToEdit = null;
  }

  deleteCustomer(index){
    let id = this.customers[index].id;
    this.customersService.deleteCustomer(this.userId, id);
  }

  predict(index){
    this.customers[index].result = 'Will default';
    this.predictService.predict(this.customers[index].education,this.customers[index].income).subscribe(
      res => {console.log(res);
      if (res > 0.5){
        var result = 'Will Pay'
      }
      else{
        var result = 'Will default'
      }
      this.customers[index].result = result}
    );
  }

  updateResult(index){
    this.customers[index].saved = true;
    this.customersService.updateResult(this.userId,this.customers[index].id,this.customers[index].result)
  }



  displayedColumns: string[] = ['name', 'Education in years', 'Personal income','Delete', 'Edit', 'Predict', 'Result'];
 
  constructor(private customersService:CustomersService,
    private authService:AuthService, private predictService:PredictService ) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();
                if(customer.result){
                  customer.saved = true;
                }
                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
              }                        
            }
          )
      })
  }   
}