import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url = "https://ah6l8bb6tb.execute-api.us-east-1.amazonaws.com/beta";

  predict(education:number,income:number){
    let json = {
      'data':{
        "edu":education,
        "inc":income
      }
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res =>{ 
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[','');
        final = final.replace(']','');
        return final;
      })
    ) 
  }

  constructor(private http:HttpClient) { }
}
