import { Customer } from './../interfaces/customer';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  @Input() name:string;
  @Input() education:number;
  @Input() income:number;
  @Input() id:string;
  @Input() type:string;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();

  
  buttonText:String = 'Add customer';
  updateParent(){
    let customer:Customer = {id:this.id,name:this.name,education:this.education,income:this.income};
    this.update.emit(customer);
    if(this.type == 'Add Customer'){
      this.name = null;
      this.education = null;
      this.income = null;
    }
  }

  tellParentToClose(){
    this.closeEdit.emit();
  }
  onSubmit(){}

  constructor() { }

  ngOnInit(): void {
  }

}
