export interface Customer {
    name: string;
    education: number;
    income: number;
    id?:string;
    saved?:Boolean;
    result?:string;     
}